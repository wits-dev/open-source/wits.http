﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WITS.Http
{
    public class CacheOptions
    {
        public readonly double ExpireSeconds;
        public readonly ExpiryBehaviour ExpireBehaviour;

        public CacheOptions(double expireSeconds, ExpiryBehaviour expiryBehaviour = ExpiryBehaviour.Sliding)
        {
            ExpireSeconds = expireSeconds;
            ExpireBehaviour = expiryBehaviour;
        }

        public enum ExpiryBehaviour
        {
            Absolute,
            Sliding
        }
    }
}
