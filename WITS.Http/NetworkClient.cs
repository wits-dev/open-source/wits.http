﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WITS.Cache;
using WITS.Http.Failure;

namespace WITS.Http
{
    public class NetworkClient : INetworkClient
    {
        protected readonly HttpClient _httpClient;
        protected readonly ILogger<NetworkClient> _logger;
        protected readonly ICacheManager _cacheManager;
        protected readonly INetworkClientDelegate _deleg;

        public NetworkClient(
            HttpClient httpClient,
            ILogger<NetworkClient> logger,
            ICacheManager cacheManager,
            INetworkClientDelegate deleg = null)
        {
            _httpClient = httpClient;
            _logger = logger;
            _cacheManager = cacheManager;
            _deleg = deleg;

            if(!_httpClient.DefaultRequestHeaders.Any(h => h.Key == "User-Agent"))
            {
                _httpClient.DefaultRequestHeaders.Add("User-Agent", "WITS NetworkClient/1.0 (CSharp)");
            }
        }

        public async Task<T> Get<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, CacheOptions cacheOptions = null)
        {
            var queryString = parameters == null ? "" : ((resource.IndexOf("?") == -1 ? "?" : "&") + string.Join("&", parameters.Select(p => $"{p.Key}={p.Value}")));

            if(!string.IsNullOrWhiteSpace(queryString))
            {
                resource += queryString;
            }

            var request = new HttpRequestMessage(HttpMethod.Get, resource);

            var cacheKey = GetCacheKey(HttpMethod.Get, resource);
            if(cacheOptions != null)
            {
                var cached = await _cacheManager.Get<string>(cacheKey).ConfigureAwait(false);

                if(cached != null)
                {
                    return JsonConvert.DeserializeObject<T>(cached);
                }
            }

            var response = await Execute(HttpMethod.Get, resource, null).ConfigureAwait(false);

            if(cacheOptions != null && response != null)
            {
                if(cacheOptions.ExpireBehaviour == CacheOptions.ExpiryBehaviour.Absolute)
                {
                    await _cacheManager.Set(cacheKey, response, DateTimeOffset.Now.AddSeconds(cacheOptions.ExpireSeconds)).ConfigureAwait(false);
                }
                else
                {
                    await _cacheManager.Set(cacheKey, response, TimeSpan.FromSeconds(cacheOptions.ExpireSeconds)).ConfigureAwait(false);
                }
            }

            return response == null ? default(T) : JsonConvert.DeserializeObject<T>(response);
        }

        public async Task Delete(string resource)
        {
            await Execute(HttpMethod.Delete, resource, null).ConfigureAwait(false);
        }

        #region form

        public Task<T> Post<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded)
        {
            return FormRequest<T>(HttpMethod.Put, resource, parameters, postMode);
        }

        public Task<T> Put<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded)
        {
            return FormRequest<T>(HttpMethod.Put, resource, parameters, postMode);
        }

        public Task<T> Patch<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded)
        {
            return FormRequest<T>(new HttpMethod("PATCH"), resource, parameters, postMode);
        }

        public Task<T> Delete<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded)
        {
            return FormRequest<T>(HttpMethod.Delete, resource, parameters, postMode);
        }

        public async Task<T> FormRequest<T>(HttpMethod method, string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded)
        {
            var content = default(Func<HttpContent>);

            if(parameters != null)
            {
                if(postMode == PostMode.FormUrlEncoded)
                {
                    content = () => new FormUrlEncodedContent(parameters);
                }
                else
                {
                    content = () =>
                    {
                        var multipart = new MultipartFormDataContent();
                        foreach(var pair in parameters)
                        {
                            multipart.Add(new StringContent(pair.Value), String.Format("\"{0}\"", pair.Key));
                        }

                        return multipart;
                    };
                }
            }

            var response = await Execute(method, resource, content).ConfigureAwait(false);
            return string.IsNullOrWhiteSpace(response) ? default(T) : JsonConvert.DeserializeObject<T>(response);
        }

        #endregion form

        #region body

        public Task<T> Post<T>(string resource, string content = null, string contentType = "application/json")
        {
            return BodyRequest<T>(HttpMethod.Post, resource, content, contentType);
        }

        public Task<T> Put<T>(string resource, string content = null, string contentType = "application/json")
        {
            return BodyRequest<T>(HttpMethod.Put, resource, content, contentType);
        }

        public Task<T> Patch<T>(string resource, string content = null, string contentType = "application/json")
        {
            return BodyRequest<T>(new HttpMethod("PATCH"), resource, content, contentType);
        }

        public Task<T> Delete<T>(string resource, string content = null, string contentType = "application/json")
        {
            return BodyRequest<T>(HttpMethod.Delete, resource, content, contentType);
        }

        public async Task<T> BodyRequest<T>(HttpMethod method, string resource, string content = null, string contentType = "application/json")
        {
            var content_ = default(Func<HttpContent>);

            if(content != null)
            {
                content_ = () => new StringContent(content, Encoding.UTF8, contentType);
            }

            var response = await Execute(method, resource, content_).ConfigureAwait(false);
            return string.IsNullOrWhiteSpace(response) ? default(T) : JsonConvert.DeserializeObject<T>(response);
        }

        #endregion body

        #region internal

        private async Task<string> Execute(HttpMethod method, string resource, Func<HttpContent> contentDelegate)
        {
            HttpResponseMessage response = null;
            Exception lastException = null;

            HttpRequestMessage request = new HttpRequestMessage(method, resource);
            try
            {
                if(contentDelegate != null)
                {
                    request.Content = contentDelegate();
                }

                await (_deleg?.OnBeforeRequest(request) ?? Task.CompletedTask).ConfigureAwait(false);

                response = await _httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead).ConfigureAwait(false);
            }
            catch(Exception e)
            {
                lastException = new EndpointException($"An error occurred while communicating with the endpoint: {_httpClient.BaseAddress}/{resource}", e);
            }

            if(response == null)
            {
                _logger.LogCritical(lastException, lastException.Message);
                throw lastException;
            }

            var requestUri = request.RequestUri;
            var statusCode = response.StatusCode;
            var reasonPhrase = response.ReasonPhrase;
            var responseHeaders = response.Headers;
            var responseBody = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            response.Dispose();

            _logger.LogTrace($"{statusCode}: {requestUri.AbsoluteUri} \n\n{responseBody}");

            if(!response.IsSuccessStatusCode)
            {
                await (_deleg?.OnFailedRequest(requestUri, statusCode, reasonPhrase, responseHeaders, responseBody) ?? Task.CompletedTask);

                if(statusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return null;
                }
                else if(statusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    throw new BadRequestError(statusCode, requestUri, responseBody, _deleg?.GetBadRequestMessages(responseBody) ?? new string[] { responseBody });
                }
                else
                {
                    throw new FailedRequestError(statusCode, requestUri, responseBody, reasonPhrase);
                }
            }

            return responseBody;
        }

        protected string Hash(string str)
        {
            string md5Hash;
            using(MD5 hash = MD5.Create())
            {
                md5Hash = string.Concat
                (
                    from ba in hash.ComputeHash(Encoding.UTF8.GetBytes(str))
                    select ba.ToString("x2")
                );
            }

            return md5Hash;
        }

        protected string GetCacheKey(HttpMethod method, string resource)
        {
            var id = $"{method}:{_httpClient.BaseAddress}{resource}";

            return "NetworkClient:Cache:" + Hash(id);
        }

        #endregion internal
    }
}