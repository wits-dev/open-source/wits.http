﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WITS.Http
{
    public interface INetworkClient
    {
        Task<T> Get<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, CacheOptions cacheOptions = null);

        Task Delete(string resource);

        Task<T> Post<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded);

        Task<T> Put<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded);

        Task<T> Patch<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded);

        Task<T> Delete<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded);

        Task<T> FormRequest<T>(HttpMethod method, string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded);

        Task<T> Post<T>(string resource, string content = null, string contentType = "application/json");

        Task<T> Put<T>(string resource, string content = null, string contentType = "application/json");

        Task<T> Patch<T>(string resource, string content = null, string contentType = "application/json");

        Task<T> Delete<T>(string resource, string content = null, string contentType = "application/json");

        Task<T> BodyRequest<T>(HttpMethod method, string resource, string content = null, string contentType = "application/json");
    }
}