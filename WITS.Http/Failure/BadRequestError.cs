﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace WITS.Http.Failure
{
    public class BadRequestError : FailedRequestError
    {
        public string[] Messages { get; private set; }

        public BadRequestError(HttpStatusCode statusCode, Uri requestUri, string responseBody, string[] messages) : base(statusCode, requestUri, responseBody, $"Bad Request: {responseBody}")
        {
            Messages = messages;
        }
    }
}
