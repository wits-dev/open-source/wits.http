﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace WITS.Http.Failure
{
    public class FailedRequestError: NetworkClientException
    {
        public HttpStatusCode StatusCode { get; protected set; }
        public string RequestUri { get; protected set; }
        public string ResponseBody { get; protected set; }

        public FailedRequestError(HttpStatusCode statusCode, Uri requestUri, string responseBody, string message) : base(message)
        {
            StatusCode = statusCode;
            RequestUri = requestUri.AbsoluteUri;
            ResponseBody = responseBody;
        }

        override public string ToString()
        {
            return $"{RequestUri} : ({StatusCode}) {Message}\n\n{ResponseBody}";
        }
    }
}
