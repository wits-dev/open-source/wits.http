﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WITS.Http.Failure
{
    public class EndpointException: NetworkClientException
    {
        public EndpointException(string message, Exception innerException) : base(message, innerException) { }
    }
}
