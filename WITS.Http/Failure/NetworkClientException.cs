﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WITS.Http.Failure
{
    public class NetworkClientException : Exception
    {
        public NetworkClientException(string message) : base(message) { }
        public NetworkClientException(string message, Exception innerException) : base(message, innerException) { }
    }
}
